/* eslint-disable no-multi-str */
module.exports = {
  apps: [
    {
      name: 'development-nestjs-demo',
      cwd: '/Volumes/Sources/JvitSources/jvit-trainning-workshops/nodejs-nest-level2',
      script: 'dist/main.js',
      max_memory_restart: '150M',
      exec_mode: 'cluster',
      instances: '8',
      autorestart: false,
      time: true,
      combine_logs: true,
      env: {
        NODE_ENV: 'staging',
      },
    },    
    {
      name: 'staging-nestjs-demo',
      cwd: '/var/www/jv-wss/staging-nestjs-demo/current',
      script: 'dist/main.js',
      max_memory_restart: '150M',
      exec_mode: 'cluster',
      instances: '1',
      autorestart: false,
      time: true,
      combine_logs: true,
      env: {
        NODE_ENV: 'staging',
      },
    },
    {
      name: 'production-nestjs-demo',
      cwd: '/var/www/jv-wss/production-nestjs-demo/current',
      script: 'dist/main.js',
      max_memory_restart: '512M',
      exec_mode: 'cluster',
      instances: 'max',
      autorestart: true,
      time: true,
      combine_logs: true,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
