FROM node:10.16.3-alpine as builder

RUN mkdir -p /var/www
RUN mkdir -p /var/ci
RUN mkdir -p ~/.ssh

# install base packages
RUN apk update && apk add curl php7 git rsync
RUN apk add composer
RUN apk add openssh-client
RUN apk add bash

# install deployer
RUN curl -LO https://deployer.org/deployer.phar
RUN mv deployer.phar /usr/local/bin/dep
RUN chmod +x /usr/local/bin/dep

# setup ci file
COPY ci /var/ci
COPY ci/ssh_config ~/.ssh/config2


# install pm2
RUN npm install -g pm2