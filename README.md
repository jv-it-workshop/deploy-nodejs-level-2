
## DEMO specs
- nodejs 16.x
- nestjs framework
- Mock API /products
- Configuration via .env
- Process management with pm2
- automation configurations using ansible
- ...

## Installation

```bash
$ yarn
```

```ansible
$ ansible-galaxy install morgangraphics.ansible_role_nvm
$ ansible-galaxy install geerlingguy.nodejs

run playbooks:
$ ansible-playbook provisioning-systems.yml -i inventory_staging
$ ansible-playbook provisioning.yml -i inventory_staging
$ ansible-playbook provisioning-webservers.yml -i inventory_staging


install intepreter
apt install python-minimal python-setuptools
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Production
- Install nestjs, pm2:
> npm install pm2 -g
> npm i -g @nestjs/cli
- build source
> nest build
- Start pm2 for production
> pm2 reload ecosystem.config.js --only {stage}-nestjs-demo

# pm2 cli
- status: `pm2 status`
- detail: `pm2 show {name}`
- check logs: `pm2 logs {name}`
- monitor simple ui: `pm2 monit`
- setup startup script `pm2 save`, after restart will auto start.

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
