import { Injectable } from '@nestjs/common';
import { PRODUCTS } from './mocks/products.mock';

@Injectable()
export class AppService {
  products = PRODUCTS;

  getHello(): string {
    return `Hello World! env: ${process.env.ENV}`;
  }

  async getProducts() {
    return await this.products;
  }

}
